package com.aanorbel.multipledatabase;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class FlywayConfig {
    public final DataSource primaryDataSource;

    public final DataSource secondaryDataSource;

    public FlywayConfig(@Qualifier("primaryDataSource") DataSource primaryDataSource,
                        @Qualifier("secondaryDataSource") DataSource secondaryDataSource) {
        this.primaryDataSource = primaryDataSource;
        this.secondaryDataSource = secondaryDataSource;
    }

    @Bean(initMethod = "migrate", name = "primaryFlyway")
    Flyway primaryFlyway() {
        Flyway flyway = new Flyway();
        flyway.setLocations("migrations/primary");
        flyway.setDataSource(primaryDataSource);
        flyway.setBaselineOnMigrate(true);
        return flyway;
    }

    @Bean(initMethod = "migrate", name = "secondaryFlyway")
    Flyway secondaryFlyway() {
        Flyway flyway = new Flyway();
        flyway.setLocations("migrations/secondary");
        flyway.setDataSource(secondaryDataSource);
        flyway.setBaselineOnMigrate(true);
        return flyway;
    }
}
